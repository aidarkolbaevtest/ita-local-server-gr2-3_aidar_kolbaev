<?php

/**
 * Defines application features from the specific context.
 */
class UserContext extends AttractorContext
{
    /**
     * @When /^я вижу слово "([^"]*)" где-то на странице$/
     */
    public function яВижуСловоГдеТоНаСтранице($arg1)
    {
        $this->assertPageContainsText($arg1);
    }

    /**
     * @When /^я нахожусь на главной странице$/
     */
    public function яНахожусьНаГлавнойСтранице()
    {
        $this->visit($this->getContainer()->get('router')->generate('ping'));
    }

    /**
     * @When /^я нахожусь на странице регистрации$/
     */
    public function яНахожусьНаСтраницеРегистрации()
    {
        $this->visit($this->getContainer()->get('router')->generate('app-sign-up'));
    }

    /**
     * @When /^я нахожусь на странице авторизации$/
     */
    public function яНахожусьНаСтраницеАвторизации()
    {
        $this->visit($this->getContainer()->get('router')->generate('app-sign-in'));
    }

    /**
     * @When /^я регистрируюсь со следующими данными email:"([^"]*)" passport: "([^"]*)" password: "([^"]*)" role: "([^"]*)"$/
     * @param $email
     * @param $passport
     * @param $password
     * @param $role
     */
    public function яРегистрируюсьСоСледующимиДанными($email, $passport, $password, $role)
    {
        $this->fillField('register_email', $email);
        $this->fillField('register_passport', $passport);
        $this->fillField('register_password', $password);
        $this->selectOption('register_roles', $role);
        $this->pressButton('register_save');
    }

    /**
     * @When /^я авторизуюсь со следующими данными email:"([^"]*)" password: "([^"]*)"$/
     */
    public function яАвторизуюсьСоСледующимиДанными($email, $password)
    {
        $this->fillField('login_email', $email);
        $this->fillField('login_password', $password);
        $this->pressButton('login_save');
    }

    /**
     * @When /^я захожу на страницу своего профиля$/
     */
    public function яЗахожуНаСтраницуСвоегоПрофиля()
    {
        $this->clickLink('Профиль');
    }


}

