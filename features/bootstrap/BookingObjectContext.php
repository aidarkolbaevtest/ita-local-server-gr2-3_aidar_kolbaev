<?php

/**
 * Defines application features from the specific context.
 */
class BookingObjectContext extends AttractorContext
{
    /**
     * @When /^я регистрирую объект со следующими данными option:"([^"]*)" name: "([^"]*)" number_of_rooms: "([^"]*)" contact_number: "([^"]*)" price: "([^"]*)"$/
     * @param $option
     * @param $name
     * @param $number_of_rooms
     * @param $contact_number
     * @param $price
     */
    public function яРегистрируюОбъектСоСледующимиДанными($option, $name, $number_of_rooms, $contact_number, $price)
    {
        $this->clickLink('register-object-button');
        $this->selectOption('object-type', $option);
        $this->getSession()->executeScript(';
            function saveCoordinates() {
                $(\'#address\').val(42.639243, 77.045479);
                        }');
        $this->fillField('object-name', $name);
        $this->fillField('number_of_rooms', $number_of_rooms);
        $this->fillField('contact_number', $contact_number);
        $this->fillField('price', $price);
        $this->pressButton('next-button');
        if ($option === 'pension') {
            $this->fillField('distance_to_coast', 500);
            $this->assertCheckboxChecked('beach');
        } else {
            $this->assertCheckboxChecked('pool');
        }
        $this->pressButton('submit-button');
    }

    /**
     * @When /^я ищу объекты с данными name: "([^"]*)" min: "([^"]*)" max: "([^"]*)"$/
     * @param $name
     * @param $min
     * @param $max
     */
    public function яИщуОбъектыСДанными($name, $min, $max)
    {
        $this->fillField('name-field', $name);
        $this->fillField('min_price', $min);
        $this->fillField('max_price', $max);
        $this->pressButton('search-button');
    }


    /**
     * @When /^я нажимаю на кнопку "([^"]*)"$/
     */
    public function яНажимаюНаКнопку($button)
    {
        $this->clickLink($button);
    }

    /**
     * @When /^я бронирую номер number: "([^"]*)"$/
     */
    public function яБронируюНомер($number)
    {
        $this->getSession()->executeScript('
            let room = $(\'#room18\');
            room.trigger(\'click\');            
        ');
        $this->pressButton('booking-button');
    }


}

