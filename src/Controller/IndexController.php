<?php

namespace App\Controller;

use App\Entity\User;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{
    /**
     * @Route("/sign-up", name="app-sign-up")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return Response
     */
    public function signUpAction(
        ApiContext $apiContext,
        Request $request,
        ObjectManager $manager,
        UserHandler $userHandler
    )
    {
        $error = null;
        $user = new User();
        $form = $this->createForm("App\Form\RegisterType", $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($apiContext->clientExists($user->getPassport(), $user->getEmail())) {
                    $error = 'Error: Вы уже зарегистрированы. Вам нужно авторизоваться.';
                } else {
                    $data = [
                        'email' => $user->getEmail(),
                        'passport' => $user->getPassport(),
                        'password' => $user->getPassword(),
                        'roles' => $user->getRoles(),
                    ];

                    $apiContext->createClient($data);

                    $user = $userHandler->createNewUser($data);
                    $manager->persist($user);
                    $manager->flush();

                    return $this->redirectToRoute("ping");
                }
            } catch (ApiException $e) {
                $error = 'Error: ' . $e->getMessage() . '  |||  ' . var_export($e->getResponse(), 1);
            }
        }

        return $this->render('sign_up.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }


    /**
     * @Route("/", name="ping")
     * @param ApiContext $apiContext
     * @return Response
     */
    public function indexAction(ApiContext $apiContext)
    {
        try {
            $apiContext->makePing();
            return $this->render('index.html.twig', [
                //
            ]);
        } catch (ApiException $e) {
            return new Response('Error: ' . $e->getMessage());
        }
    }

    /**
     * @Route("/sign-in", name="app-sign-in")
     * @param UserRepository $userRepository
     * @param Request $request
     * @param UserHandler $userHandler
     * @param ApiContext $apiContext
     * @param ObjectManager $manager
     * @return Response
     * @throws ApiException
     */
    public function authAction(
        UserRepository $userRepository,
        Request $request,
        UserHandler $userHandler,
        ApiContext $apiContext,
        ObjectManager $manager
    )
    {
        $error = null;

        $form = $this->createForm("App\Form\AuthType");

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $user = $userRepository->getByCredentials(
                $data['password'],
                $data['email']
            );

            if ($user) {
                $userHandler->makeUserSession($user);
                return $this->redirectToRoute('ping');
            }

            try {
                if ($apiContext->checkClientCredentials(
                    $data['password'],
                    $data['email']
                )) {
                    $centralData = $apiContext->getClientByEmail($data['email']);
                    $centralData['roles'][] = 'ROLE_USER';
                    $user = $userHandler->createNewUser(
                        $centralData,
                        false
                    );
                    $manager->persist($user);
                    $manager->flush();
                    $userHandler->makeUserSession($user);
                    return $this->redirectToRoute('ping');
                } else {
                    $error = 'Ты не тот, за кого себя выдаешь';
                }
            } catch (ApiException $e) {
                $error = 'Что-то где-то пошло нетак';
            }
        }

        return $this->render(
            '/sign_in.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/login/{afterDenied}", name="login")
     * @param null/string $afterDenied
     * @return Response
     */
    public function loginAction($afterDenied = null)
    {
        if ($afterDenied) {
            return new Response('Ошибка - доступа нет');
        }

        return new Response('доступа есть');
    }


    /**
     * @Route("/booking_object", name="app-booking-object-register")
     * @Method("POST")
     * @param Request $request
     * @param ApiContext $apiContext
     * @return JsonResponse
     */
    public function bookingObjectRegisterAction(Request $request, ApiContext $apiContext) {
        $data['type'] = $request->request->get('type');
        $data['name'] = $request->request->get('name');
        $data['number_of_rooms'] = $request->request->get('number_of_rooms');
        $data['contact_number'] = $request->request->get('contact_number');
        $data['address'] = $request->request->get('address');
        $data['price'] = $request->request->get('price');
        $data['email'] = $request->request->get('email');
        $data['garage'] = $request->request->get('garage');
        $data['pool'] = $request->request->get('pool');
        $data['beach'] = $request->request->get('beach');
        $data['distance_to_coast'] = $request->request->get('distance_to_coast');
        try {
            if($apiContext->bookingObjectExists($data['name'], $data['contact_number'])) {
                return new JsonResponse(['error' => 'Такой объект бронирования уже существует'], 404);
            } else {
                $apiContext->bookingObjectCreate($data);
                return new JsonResponse(['result' => 'ok']);
            }
        } catch (ApiException $e) {
            return new JsonResponse(['error' => $e->getMessage() . '|||' . $e->getResponse()], 406);
        }
    }


    /**
     * @Route("/booking_object", name="app-booking-object-search")
     * @Method("GET")
     * @param Request $request
     * @param ApiContext $apiContext
     * @return JsonResponse
     */
    public function bookingObjectsSearchAction(Request $request, ApiContext $apiContext) {
        $data['name'] = $request->query->get('name');
        $data['min_price'] = $request->query->get('min_price');
        $data['max_price'] = $request->query->get('max_price');
        $data['type'] = $request->query->get('type');
        $data['options'] = [
            'garage' => $request->query->get('garage'),
            'pool' => $request->query->get('pool'),
            'beach' => $request->query->get('beach'),
            'distance_to_coast' => $request->query->get('distance_to_coast'),
        ];
        try {
            $booking_objects = $apiContext->getBookingObjects($data);
            return new JsonResponse($booking_objects);
        } catch (ApiException $e) {
            return new JsonResponse(['error' => 'Что-то пошло не так'], 406);
        }
    }


    /**
     * @Route("/booking_object/details/{id}", requirements={"id": "\d+"},name="app-booking-object-details")
     * @param int $id
     * @param ApiContext $apiContext
     * @return Response
     */
    public function bookingObjectDetailsAction(int $id, ApiContext $apiContext) {
        $error = null;
        $bookingObject = null;
        try {
            if($apiContext->checkBooking($this->getUser()->getEmail())) {
                return $this->redirectToRoute('ping');
            };
            $bookingObject = $apiContext->getBookingObjectById($id);
            for ($i = 0; $i < count($bookingObject['busy_rooms']); $i++) {
                $dates[$i] = $bookingObject['busy_rooms'][$i]['expiration_date']['date'];
                $busy_rooms[$i] = $bookingObject['busy_rooms'][$i]['room'];
            }
        } catch (ApiException $e) {
            $error = 'Что-то пошло не так';
        }
        return $this->render(
            'details.html.twig', [
            'object' => (object) $bookingObject,
            'error' => $error,
            'busy_rooms' => $busy_rooms ?? null,
            'dates' => $dates ?? null
        ]);
    }

    /**
     * @Method("POST")
     * @Route("/booking", name="app-booking")
     * @param Request $request
     * @param ApiContext $apiContext
     * @return JsonResponse
     */
    public function bookingAction(Request $request, ApiContext $apiContext) {
        $data['object'] = $request->request->get('object');
        $data['tenant'] = $request->request->get('tenant');
        $data['room'] = $request->request->get('room');
        if (empty($data['object']) || empty($data['tenant']) || empty($data['room'])) {
            return new JsonResponse(['error' => 'Всё плохо'], 400);
        }
        try {
            $result = $apiContext->booking($data);
            return new JsonResponse($result);
        } catch (ApiException $e) {
            return new JsonResponse($e->getResponse() . '---' . $e->getMessage(), 500);
        }
    }


    /**
     * @Route("/profile", name="app-profile")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function profileAction() {
        return $this->render('profile.html.twig', [
           //
        ]);
    }


    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction()
    {
        $this->get('security.token_storage')->setToken(null);
        $this->container->get('session')->invalidate();
        return $this->redirectToRoute('ping');
    }
}
