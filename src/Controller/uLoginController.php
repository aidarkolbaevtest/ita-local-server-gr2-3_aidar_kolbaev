<?php

namespace App\Controller;

use App\Entity\User;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class uLoginController extends Controller
{
    const ULOGIN_DATA = 'ulogin-data';

    /**
     * @Route("/register-case", name="app-register-case")
     */
    public function registerCaseAction()
    {
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $userData = json_decode($s, true);
        dump($userData);
        $user = new User();
        if (empty($userData['email'])) {
            $error = 'У вас не указан email адрес в данной соц. сети!';
        } else {
            $error = null;
            $user->setEmail($userData['email']);
        }

        $form = $this->createForm(
            'App\Form\ULoginRegisterType', $user, [
                'action' => $this
                    ->get('router')
                    ->generate('app-register-case2')
            ]
        );
        //$user['network'] - соц. сеть, через которую авторизовался пользователь
        //$user['identity'] - уникальная строка определяющая конкретного пользователя соц. сети
        //$user['first_name'] - имя пользователя
        //$user['last_name'] - фамилия пользователя
        $this->get('session')->set(self::ULOGIN_DATA, $s);

        return $this->render('registration/ul_2_state.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }


    /**
     * @Route("/profile/bind", name="bind_profile")
     * @param ObjectManager $manager
     * @param ApiContext $apiContext
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function bindAccountAction(ObjectManager $manager, ApiContext $apiContext) {
        $user = $this->getUser();
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $userData = json_decode($s, true);
        $network = $userData['network'];
        $uid = $userData['uid'];
        try {
            $apiContext->updateClient($user->getEmail(),[
                'network' => [
                    $network => $uid
                ]
            ]);
            $user->setSocialId($network, $uid);
            $manager->flush();
            return $this->redirectToRoute('app-profile');
        } catch (ApiException $e) {
            return new Response('Error: ' . $e->getMessage());
        }
    }


    /**
     * @Route("/sign-in-social-network", name="sign_in-via-social-network")
     * @param ObjectManager $manager
     * @param UserRepository $userRepository
     * @param UserHandler $userHandler
     * @param ApiContext $apiContext
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function signInViaSocialNetwork(
        ObjectManager $manager,
        UserRepository $userRepository,
        UserHandler $userHandler,
        ApiContext $apiContext
    ) {
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $userData = json_decode($s, true);
        $network = $userData['network'];
        $uid = $userData['uid'];
        if ($network == $userHandler::SOC_NETWORK_VKONTAKTE) {
            $network = 'vk';
        }
        $user = $userRepository->getBySocialNetwork($network, $uid);
        if ($user) {
            $userHandler->makeUserSession($user);
            return $this->redirectToRoute('ping');
        }
        try {
            $centralData = $apiContext->getClientBySocialNetwork($network, $uid);
            $user = $userRepository->findOneByPassportOrEmail($centralData['passport'], $centralData['email']);
            if ($user) {
                $user->setVkId($centralData['vkId']);
                $user->setFaceBookId($centralData['faceBookId']);
                $user->setGoogleId($centralData['googleId']);
            } else {
                $user = $userHandler->createNewUser($centralData, true);
            }
            $manager->persist($user);
            $manager->flush();
            $userHandler->makeUserSession($user);
            return $this->redirectToRoute('ping');
        } catch (ApiException $e) {
            $error = 'Вы еще не регистрировались у нас, либо не привязали данную соц сеть к аккаунту';
            return new Response($error);
        }
    }

    /**
     * @Route("/register-case2", name="app-register-case2")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function registerCase2Action(
        ApiContext $apiContext,
        Request $request,
        ObjectManager $manager,
        UserHandler $userHandler
    )
    {
        $userData = json_decode(
            $this
                ->get('session')
                ->get(self::ULOGIN_DATA),
            true
        );

        $user = new User();

        $form = $this->createForm(
            'App\Form\ULoginRegisterType',
            $user
        );

        $form->handleRequest($request);

        $error = null;
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($apiContext->clientExists($user->getPassport(), $user->getEmail())) {
                    $error = 'Error: Вы уже зарегистрированы. Вам нужно авторизоваться.';
                } else {
                    $user->setPassword("social------------------------".time());
                    $user->setSocialId($userData['network'], $userData['uid']);
                    $data = $user->__toArray();

                    $apiContext->createClient($data);

                    $user = $userHandler->createNewUser($data);

                    $manager->persist($user);
                    $manager->flush();

                    return $this->redirectToRoute("ping");
                }
            } catch (ApiException $e) {
                $error = 'Error: ' . $e->getMessage() . '  |||  ' . var_export($e->getResponse(), 1);
            }
        }

        return $this->render('registration/ul_2_state.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }
}