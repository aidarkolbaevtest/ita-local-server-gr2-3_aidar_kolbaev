<?php

namespace App\Model\Api;


use Curl\Curl;
use Symfony\Bundle\FrameworkBundle\Tests\CacheWarmer\testRouterInterfaceWithoutWarmebleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ApiContext extends AbstractApiContext
{
    const ENDPOINT_PING = '/ping';
    const ENDPOINT_CLIENT = '/client';
    const ENDPOINT_CLIENT_PASSWORD_ENCODE = '/client/password/encode';
    const ENDPOINT_CONCRETE_CLIENT_BY_EMAIL = '/client/email/{email}';
    const ENDPOINT_CONCRETE_CLIENT = '/client/{passport}/{email}';
    const ENDPOINT_CHECK_CLIENT_CREDENTIALS = '/check_client_credentials/{encodedPassword}/{email}';
    const ENDPOINT_CONCRETE_CLIENT_BY_SOCIAL_NETWORK = '/client/social_network/{network}/{uid}';
    const ENDPOINT_CONCRETE_BOOKING_OBJECT = '/booking_object/{name}/{contact_number}';
    const ENDPOINT_BOOKING_OBJECT = '/booking_object';
    const ENDPOINT_BOOKING_OBJECT_BY_ID = '/booking_object/{id}';
    const ENDPOINT_BOOKING = '/booking';
    const ENDPOINT_CHECK_BOOKING = '/booking/{email}';

    /**
     * @return mixed
     * @throws ApiException
     */
    public function makePing()
    {
        return $this->makeQuery(self::ENDPOINT_PING, self::METHOD_GET);
    }


    /**
     * @param string $email
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function updateClient(string $email, array $data) {
        $endPoint = $this->generateApiUrl(self::ENDPOINT_CONCRETE_CLIENT_BY_EMAIL, [
            'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_PATCH, $data);
    }


    /**
     * @param string $network
     * @param string $uid
     * @return mixed
     * @throws ApiException
     */
    public function getClientBySocialNetwork(string $network, string $uid) {
        $endPoint = $this->generateApiUrl(self::ENDPOINT_CONCRETE_CLIENT_BY_SOCIAL_NETWORK, [
            'network' => $network,
            'uid' => $uid
        ]);

        return $this->makeQuery($endPoint, self::METHOD_GET);
    }



    /**
     * @param string $passport
     * @param string $email
     * @return bool
     * @throws ApiException
     */
    public function clientExists(string $passport, string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CONCRETE_CLIENT, [
                'passport' => $passport,
                'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }


    /**
     * @param string $email
     * @return array
     * @throws ApiException
     */
    public function getClientByEmail(string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CONCRETE_CLIENT_BY_EMAIL, [
            'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_GET);
    }

    /**
     * @param string $plainPassword
     * @param string $email
     * @return bool
     * @throws ApiException
     */
    public function checkClientCredentials(string $plainPassword, string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CHECK_CLIENT_CREDENTIALS, [
            'encodedPassword' => $this->encodePassword($plainPassword),
            'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function createClient(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_CLIENT, self::METHOD_POST, $data);
    }

    /**
     * @param string $password
     * @return mixed
     * @throws ApiException
     */
    public function encodePassword(string $password)
    {
        $response = $this->makeQuery(self::ENDPOINT_CLIENT_PASSWORD_ENCODE, self::METHOD_GET, [
            'plainPassword' => $password
        ]);

        return $response['result'];
    }


    /**
     * @param string $name
     * @param string $contact_number
     * @return mixed
     * @throws ApiException
     */
    public function bookingObjectExists(string $name, string $contact_number)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CONCRETE_BOOKING_OBJECT, [
            'name' => $name,
            'contact_number' => $contact_number
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }


    /**
     * @param $data
     * @return mixed
     * @throws ApiException
     */
    public function getBookingObjects($data) {
        return $this->makeQuery(self::ENDPOINT_BOOKING_OBJECT, self::METHOD_GET, $data);
    }


    /**
     * @param int $id
     * @return mixed
     * @throws ApiException
     */
    public function getBookingObjectById(int $id) {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_BOOKING_OBJECT_BY_ID, [
            'id' => $id
        ]);

        return $this->makeQuery($endPoint, self::METHOD_GET);
    }


    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function booking(array $data) {
        return $this->makeQuery(self::ENDPOINT_BOOKING, self::METHOD_POST, $data);
    }


    /**
     * @param string $email
     * @return mixed
     * @throws ApiException
     */
    public function checkBooking(string $email) {
        $endPoint = $this->generateApiUrl(self::ENDPOINT_CHECK_BOOKING, [
                'email' => $email
        ]);
        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function bookingObjectCreate(array $data) {
        return $this->makeQuery(self::ENDPOINT_BOOKING_OBJECT, self::METHOD_POST, $data);
    }
}
