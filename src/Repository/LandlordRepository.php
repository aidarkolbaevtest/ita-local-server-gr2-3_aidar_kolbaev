<?php

namespace App\Repository;

use App\Entity\Landlord;

/**
 * @method Landlord|null find($id, $lockMode = null, $lockVersion = null)
 * @method Landlord|null findOneBy(array $criteria, array $orderBy = null)
 * @method Landlord[]    findAll()
 * @method Landlord[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LandlordRepository extends UserRepository
{

}
