<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TenantRepository")
 */
class Tenant extends User
{
    /**
     * @var string
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    private $bookingObjectName;

    /**
     * @param string $bookingObjectName
     * @return Tenant
     */
    public function setBookingObjectName(?string $bookingObjectName): Tenant
    {
        $this->bookingObjectName = $bookingObjectName;
        return $this;
    }


    public function getBookingObjectName(): ?string
    {
        return $this->bookingObjectName;
    }

}
