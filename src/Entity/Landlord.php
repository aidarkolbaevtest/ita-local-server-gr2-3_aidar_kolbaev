<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LandlordRepository")
 */
class Landlord extends User
{
    /**
     * @var array
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $bookingObjectsNames = [];

    /**
     * @return array
     */
    public function getBookingObjectsNames(): ?array
    {
        return $this->bookingObjectsNames;
    }

    /**
     * @param mixed $bookingObjectsNames
     * @return Landlord
     */
    public function setBookingObjectsNames(mixed $bookingObjectsNames): Landlord
    {
        $this->bookingObjectsNames = $bookingObjectsNames;
        return $this;
    }

    /**
     * @param string $bookingObjectName
     */
    public function addBookingObjectName(string $bookingObjectName) {
        $this->bookingObjectsNames[] = $bookingObjectName;
    }

}
